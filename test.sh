#!/bin/bash

ENTRADA="$1"

COR="$(curl --location 'https://dev.knin-project.online/mensagem' \
    --resolve "dev.knin-project.online:443:10.100.68.81" \
    --header 'Host: dev.knin-project.online' \
    --cacert certs/knin-project.online.crt --key certs/"$ENTRADA".key --cert certs/"$ENTRADA".crt |
    jq '.valor' | tr -d \")"

if [ "$COR" == "$ENTRADA" ]; then
    echo "OK"
else
    echo "Não foi possivel concluir teste"
fi
