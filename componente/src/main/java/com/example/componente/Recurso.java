package com.example.componente;


import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("mensagem")
class Recurso {

    private final Repositorio repositorio;

    Recurso(Repositorio repositorio) {
        this.repositorio = repositorio;
    }

    @GetMapping
    public Mensagem getMensagem() {
        return repositorio.findAll().stream().findFirst().orElseThrow();
    }

}
