package com.example.componente;

import jakarta.persistence.*;

@Entity
@Table(name = "mensagem")
public class Mensagem {

    public Mensagem() {

    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "valor")
    private String valor;

    public Long getId() {
        return id;
    }

    public String getValor() {
        return valor;
    }
}
