package com.example.componente;

import org.springframework.data.jpa.repository.JpaRepository;

public interface Repositorio extends JpaRepository<Mensagem, Long> {

}
