package com.example.componente;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ComponenteApplication {

    public static void main(String[] args) {
        SpringApplication.run(ComponenteApplication.class, args);
    }

}
