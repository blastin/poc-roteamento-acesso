#!/bin/bash

sed "s/COR/$1/g;s/ENV/$1/g" deployment.yaml >"build/$1.yaml"

kubectl apply -f "build/$1.yaml"
