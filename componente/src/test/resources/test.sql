drop table if exists mensagem;

create table mensagem
(
    id    bigint not null,
    valor varchar(20),
    PRIMARY KEY (id)
);

insert into mensagem(valor, id)
VALUES ('vermelho', 1);