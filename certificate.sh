#!/bin/bash

DOMINIO=${1:-"knin-project.online"}
PASTA=${2:-"certs"}

mkdir -p "$PASTA"

# CREATE KEY ROOT
#openssl genrsa -out "$PASTA/$DOMINIO.key" 2048

#CRIANDO CERTIFICADO DE AUTORIZAÇÃO
#openssl req -new -x509 -days 3650 -subj "/C=BR/ST=SP/CN=$DOMINIO" -addext "subjectAltName= DNS:*.$DOMINIO" -key "$PASTA/$DOMINIO.key" -out "$PASTA/$DOMINIO.crt"

#CRIANDO CERTIFICADO DO SERVIDOR
#openssl genrsa -out "$PASTA/dev.$DOMINIO.key" 2048
#openssl req -new -key "$PASTA/dev.$DOMINIO.key" -out "$PASTA/dev.$DOMINIO.csr" -subj "/CN=dev.$DOMINIO"
#openssl x509 -req -in "$PASTA/dev.$DOMINIO.csr" -CA "$PASTA/$DOMINIO.crt" -CAkey "$PASTA/$DOMINIO.key" -set_serial 344343 -out "$PASTA/dev.$DOMINIO.crt" -days 365 -sha256

# CRIANDO CERTIIFICADO E CHAVE DO CLIENT VERMELHO
openssl genrsa -out "$PASTA/vermelho.key" 2048
openssl req -new -key "$PASTA/vermelho.key" -out "$PASTA/vermelho.csr" -subj "/CN=vermelho"
openssl x509 -req -in "$PASTA/vermelho.csr" -CA "$PASTA/$DOMINIO.crt" -CAkey "$PASTA/$DOMINIO.key" -set_serial 344343 -out "$PASTA/vermelho.crt" -days 30 -sha256
openssl pkcs12 -export -out "$PASTA/vermelho.p12" -inkey "$PASTA/vermelho.key" -in "$PASTA/vermelho.crt" -certfile "$PASTA/$DOMINIO.crt" -password pass:vermelho

# CRIANDO CERTIIFICADO E CHAVE DO CLIENT AZUL
openssl genrsa -out "$PASTA/azul.key" 2048
openssl req -new -key "$PASTA/azul.key" -out "$PASTA/azul.csr" -subj "/CN=azul"
openssl x509 -req -in "$PASTA/azul.csr" -CA "$PASTA/$DOMINIO.crt" -CAkey "$PASTA/$DOMINIO.key" -set_serial 344343 -out "$PASTA/azul.crt" -days 30 -sha256
openssl pkcs12 -export -out "$PASTA/azul.p12" -inkey "$PASTA/azul.key" -in "$PASTA/azul.crt" -certfile "$PASTA/$DOMINIO.crt" -password pass:azul

# CRIANDO CERTIIFICADO E CHAVE DO CLIENT AMARELO
openssl genrsa -out "$PASTA/amarelo.key" 2048
openssl req -new -key "$PASTA/amarelo.key" -out "$PASTA/amarelo.csr" -subj "/CN=amarelo"
openssl x509 -req -in "$PASTA/amarelo.csr" -CA "$PASTA/$DOMINIO.crt" -CAkey "$PASTA/$DOMINIO.key" -set_serial 344343 -out "$PASTA/amarelo.crt" -days 30 -sha256
openssl pkcs12 -export -out "$PASTA/amarelo.p12" -inkey "$PASTA/amarelo.key" -in "$PASTA/amarelo.crt" -certfile "$PASTA/$DOMINIO.crt" -password pass:amarelo

rm "$PASTA"/*.csr
