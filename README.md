# Tutorial

## Helm

```sh
helm repo add istio https://istio-release.storage.googleapis.com/charts
helm repo update
```

## Subir uma instancia de kubernetes [ v 1.26.1]

```sh
minikube start
minikube addons enable metrics-server
minikube addons enable dashboard
kubectl create namespace istio-system
helm install istio-base istio/base -n istio-system
helm install istiod istio/istiod -n istio-system
kubectl create namespace istio-ingress
helm install istio-ingressgateway istio/gateway -n istio-ingress
kubectl create -n istio-ingress secret generic knin-project-secret \
--from-file=tls.key=certs/dev.knin-project.online.key \
--from-file=tls.crt=certs/dev.knin-project.online.crt \
--from-file=ca.crt=certs/knin-project.online.crt
kubectl create namespace dev
kubectl label namespace dev istio-injection=enabled
```

### Observação [ Tunnel de conexão]

```sh
minikube tunnel -c
```

---

## Backend

```
JAVA_HOME=~/.jdks/graalvm-ce-17 ./mvnw clean spring-boot:build-image -Pnative
```
